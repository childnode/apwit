<?php
/*
namespace aPWIT;

set_error_handler('aPWIT\error\handler\error::get');
set_exception_handler('aPWIT\error\handler\exception::get');
*/

require_once('handler/Error.php');
require_once('handler/Exception.php');

class aPWIT
{
    private static $_instance     = null;
    private $registeredFrontends  = array();

    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function __construct()
    {
    }

    public function getRegisteredFrontends()
    {
        return $this->registeredFrontends;
    }

    public static function isCLI()
    {
        if (defined('STDIN') && is_resource(STDIN)) {
            return true;
        }
        return false;
    }
    
    public static function getButton(aPWIT_FormElement $parent, aPWIT_Frontend $frontend)
    {
        return aPWIT_Component_Factory::Button($parent, $frontend);
    }

    public function start(aPWIT_Frontend $frontend)
    {
        $this->registeredFrontends[] = $frontend;
        return count($this->registeredFrontends[]);
    }
}

class aPWIT_Component_Factory
{
    public static function Button(aPWIT_FormElement $parent, aPWIT_Frontend $frontend)
    {
        
    }
}

interface aPWIT_FormElement
{
}

interface aPWIT_LayoutElement
{
}

interface aPWIT_FormElementReaction
{
}

interface aPWIT_Frontend
{
}
