<?php
error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', 1);
if (version_compare(PHP_VERSION, '5.2.0', '<')) {
    throw new Exception('AT LEAST PHP 5.2.0 is needed to use this awful aPWIT Framework, you have v' . PHP_VERSION);
}
require_once('apwit.php');

apwit::getInstance();
