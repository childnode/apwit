<?php
require_once('libs/FirePHPCore/fb.php');
$firephp = FirePHP::getInstance(true)->registerErrorHandler();

#set_error_handler(array('aPWIT_handler_Error', 'show'));
class aPWIT_handler_Error
{
    static public function show(/*int*/ $errno, /*string*/ $errstr, /*string*/ $errfile = '', /*int*/ $errline = 0, Array $errcontext = array())
    {
        print "An Error occurred, check FirePHP";
    }
}
